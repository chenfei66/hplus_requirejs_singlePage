此框架是基于requirejs的，界面是改装hplus，优化了文件组织结构，废弃原hplus的iframe，用requirejs-text插件加载html，改装成了单页应用；并用npm管理了前端开发常用的一些jquery插件。

# 框架运行需要进入package.json目录npm install一下，项目启动运行有跨域问题，需要解决浏览器跨域或者全部丢到tomcat下面。

说明：
	此系统是多窗口系统，原来的hplus+是采用的iframe窗口作为页面的载体，不但有大量冗余代码，而且性能也会有一定影响。改成requirejs加载html后，页面分四个状态（init：第一次打开页面，in：切入页面，out：切出页面，destroy：关闭页面），页面的作用域暴露在`window.scope`上面、模块`return`的对象属性会直接暴露在window上面，从而可以进行事件的绑定。

│  favicon.ico  
│  index.html			`入口文件`  
│  package.json			`包管理`  
│  README.md  
│    
├─common  
│  ├─css  
│  │      common.css	`公共样式`  
│  │      style.css		`hplus样式`  
│  │        
│  ├─img  
│  │      systemLogo.png  
│  │        
│  └─js  
│          common.js 	`公共方法`  
│          hplus.js 	`hplus控件js`  
│          source.js 	`data-main`  
│          
├─node_modules			`第三方lib`  
│                
└─pages					`模块（页面），内有模块css、html、js以及测试用的数据`
----------------------------------------------------------------------------------------------
![软件截图](https://git.oschina.net/uploads/images/2017/0930/151223_28621832_738769.png "QQ截图20170930151216.png")
