function NavToggle() {
    $(".navbar-minimalize").trigger("click");
}

function SmoothlyMenu() {
    $("body").hasClass("mini-navbar") ? $("body").hasClass("fixed-sidebar") ? ($("#side-menu").hide(), setTimeout(function() {
        $("#side-menu").fadeIn(500);
    }, 300)) : $("#side-menu").removeAttr("style") : ($("#side-menu").hide(), setTimeout(function() {
        $("#side-menu").fadeIn(500);
    }, 100));
}

$("#side-menu").metisMenu();

$(".sidebar-collapse").slimScroll({
    height: "100%",
    railOpacity: .9,
    alwaysVisible: !1
});

$(".navbar-minimalize").click(function() {
    $("body").toggleClass("mini-navbar");
    SmoothlyMenu();
});

$("#side-menu>li").click(function() {
    $("body").hasClass("mini-navbar") && NavToggle();
});

$("#side-menu>li li a").click(function() {
    $(window).width() < 769 && NavToggle();
});

$(".nav-close").click(NavToggle);

/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent) && $("#content-main").css("overflow-y", "auto");

$(window).bind("load resize", function() {
    $(this).width() < 769 && ($("body").addClass("mini-navbar"), $(".navbar-static-side").fadeIn());
});

function spage(src, status) {
    var pageName = src.split('/').pop().split('.')[0];
    router({
        pageName: pageName,
        status: status,
        el: $('.J_iframe[src="' + src + '"]')
    });
}

function selectTab(k, el) {
    var a_in = el || $('a.J_menuItem[href="' + k + '"]');
    if (k) {
        $(".J_menuItem").removeAttr("style");
        a_in.css({
            "background": "#39aef5",
            "color": "#fff"
        });
    }
    var isIn = a_in.parent().parent().hasClass("in");
    var a_ok = a_in.parent().parent().prev();
    if (!isIn) {
        a_ok.parent("li").addClass("active").children("ul").addClass('in');
    }
    var parent_a = a_ok.parent().parent().prev();
    if (parent_a.length > 0 && parent_a[0].tagName.toUpperCase() == 'A') {
        selectTab('', a_ok);
    }
}

$('#content-main').on('click', '.collapse-link', function() {
    var o = $(this).closest("div.ibox"),
        e = $(this).find("i"),
        i = o.find("div.ibox-content");
    i.slideToggle(200);
    e.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
    o.toggleClass("").toggleClass("border-bottom");
    setTimeout(function() {
        o.resize();
        o.find("[id^=map-]").resize();
    }, 50);
});

$('#content-main').on('click', '.close-link', function() {
    var o = $(this).closest("div.ibox");
    o.remove();
});

var homePage = $('div[name="iframe0"]').attr('src');
spage(homePage, 'init');
selectTab(homePage);

function f(l) {
    var k = 0;
    $(l).each(function() {
        k += $(this).outerWidth(true);
    });
    return k;
}

function g(n) {
    var o = f($(n).prevAll()),
        q = f($(n).nextAll());
    var l = f($(".content-tabs").children().not(".J_menuTabs"));
    var k = $(".content-tabs").outerWidth(true) - l;
    var p = 0;
    if ($(".page-tabs-content").outerWidth() < k) {
        p = 0;
    } else {
        if (q <= (k - $(n).outerWidth(true) - $(n).next().outerWidth(true))) {
            if ((k - $(n).next().outerWidth(true)) > q) {
                p = o;
                var m = n;
                while ((p - $(m).outerWidth()) > ($(".page-tabs-content").outerWidth() - k)) {
                    p -= $(m).prev().outerWidth();
                    m = $(m).prev();
                }
            }
        } else {
            if (o > (k - $(n).outerWidth(true) - $(n).prev().outerWidth(true))) {
                p = o - $(n).prev().outerWidth(true);
            }
        }
    }
    $(".page-tabs-content").animate({
        marginLeft: 0 - p + "px"
    }, "fast");
}

function a() {
    var o = Math.abs(parseInt($(".page-tabs-content").css("margin-left")));
    var l = f($(".content-tabs").children().not(".J_menuTabs"));
    var k = $(".content-tabs").outerWidth(true) - l;
    var p = 0;
    if ($(".page-tabs-content").width() < k) {
        return false;
    } else {
        var m = $(".J_menuTab:first");
        var n = 0;
        while ((n + $(m).outerWidth(true)) <= o) {
            n += $(m).outerWidth(true);
            m = $(m).next();
        }
        n = 0;
        if (f($(m).prevAll()) > k) {
            while ((n + $(m).outerWidth(true)) < (k) && m.length > 0) {
                n += $(m).outerWidth(true);
                m = $(m).prev();
            }
            p = f($(m).prevAll());
        }
    }
    $(".page-tabs-content").animate({
        marginLeft: 0 - p + "px"
    }, "fast");
}

function b() {
    var o = Math.abs(parseInt($(".page-tabs-content").css("margin-left")));
    var l = f($(".content-tabs").children().not(".J_menuTabs"));
    var k = $(".content-tabs").outerWidth(true) - l;
    var p = 0;
    if ($(".page-tabs-content").width() < k) {
        return false;
    } else {
        var m = $(".J_menuTab:first");
        var n = 0;
        while ((n + $(m).outerWidth(true)) <= o) {
            n += $(m).outerWidth(true);
            m = $(m).next();
        }
        n = 0;
        while ((n + $(m).outerWidth(true)) < (k) && m.length > 0) {
            n += $(m).outerWidth(true);
            m = $(m).next();
        }
        p = f($(m).prevAll());
        if (p > 0) {
            $(".page-tabs-content").animate({
                marginLeft: 0 - p + "px"
            }, "fast");
        }
    }
}
$(".J_menuItem").each(function(k) {
    if (!$(this).attr("data-index")) {
        $(this).attr("data-index", k);
    }
});

function c() {
    var o = $(this).attr("href"),
        m = $(this).data("index"),
        l = $.trim($(this).text()),
        k = true;
    if (o == undefined || $.trim(o).length == 0) {
        return false;
    }
    $(".J_menuTab").each(function() {
        if ($(this).data("id") == o) {
            if (!$(this).hasClass("active")) {
                var i = $(this).parent().find(".J_menuTab.active").data("id");
                spage(i, "out");
                $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
                g(this);
                $(".J_mainContent .J_iframe").each(function() {
                    if ($(this).data("id") == o) {
                        $(this).show().siblings(".J_iframe").hide();
                        spage(o, "in");
                        return false;
                    }
                })
            }
            k = false;
            return false;
        }
    });
    if (k) {
        spage($(".J_menuTab.active").data("id"), "out");
        var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + o + '">' + l + ' <i class="fa fa-times-circle"></i></a>';
        $(".J_menuTab").removeClass("active");
        var n = $('<div class="J_iframe" name="iframe' + m + '" width="100%" height="100%" src="' + o + '" frameborder="0" data-id="' + o + '" seamless></div>');
        $(".J_mainContent").find("div.J_iframe").hide().parents(".J_mainContent").append(n);
        $(".J_menuTabs .page-tabs-content").append(p);
        g($(".J_menuTab.active"));
        spage(o, "init");
    }
    $(".J_menuItem").removeAttr("style");
    $(this).css({
        "background": "#39aef5",
        "color": "#fff"
    });
    return false;
}
$(".J_menuItem").on("click", c);

function h() {
    var m = $(this).parents(".J_menuTab").data("id");
    var l = $(this).parents(".J_menuTab").width();
    if ($(this).parents(".J_menuTab").hasClass("active")) {
        if ($(this).parents(".J_menuTab").next(".J_menuTab").size()) {
            var k = $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").data("id");
            $(this).parents(".J_menuTab").next(".J_menuTab:eq(0)").addClass("active");
            $(".J_mainContent .J_iframe").each(function() {
                if ($(this).data("id") == k) {
                    $(this).show().siblings(".J_iframe").hide();
                    return false;
                }
            });
            var n = parseInt($(".page-tabs-content").css("margin-left"));
            if (n < 0) {
                $(".page-tabs-content").animate({
                    marginLeft: (n + l) + "px"
                }, "fast");
            }
            $(this).parents(".J_menuTab").remove();
            $(".J_mainContent .J_iframe").each(function() {
                if ($(this).data("id") == m) {
                    $(this).remove();
                    return false;
                }
            });
            selectTab(k);
        }
        if ($(this).parents(".J_menuTab").prev(".J_menuTab").size()) {
            var k = $(this).parents(".J_menuTab").prev(".J_menuTab:last").data("id");
            $(this).parents(".J_menuTab").prev(".J_menuTab:last").addClass("active");
            $(this).parents(".J_menuTab").remove();
            $(".J_mainContent .J_iframe").each(function() {
                if ($(this).data("id") == m) {
                    $(this).remove();
                    spage(m, 'destroy');
                    return false;
                }
            });
            $(".J_mainContent .J_iframe").each(function() {
                if ($(this).data("id") == k) {
                    $(this).show().siblings(".J_iframe").hide();
                    spage(k, "in");
                    return false;
                }
            });
            selectTab(k);
        }
    } else {
        $(this).parents(".J_menuTab").remove();
        $(".J_mainContent .J_iframe").each(function() {
            if ($(this).data("id") == m) {
                $(this).remove();
                spage(m, 'destroy');
                return false;
            }
        });
        g($(".J_menuTab.active"));
    }
    return false;
}
$(".J_menuTabs").on("click", ".J_menuTab i", h);

function i() {
    $(".page-tabs-content").children("[data-id]").not(":first").not(".active").each(function() {
        var dataId = $(this).data("id");
        var iframe = $('.J_iframe[data-id="' + dataId + '"]');
        iframe.remove();
        $(this).remove();
        spage(dataId, 'destroy');
    });
    $(".page-tabs-content").css("margin-left", "0");
}
$(".J_tabCloseOther").on("click", i);

function j() {
    g($(".J_menuTab.active"));
}
$(".J_tabShowActive").on("click", j);

function e() {
    var self = this;
    var k = $(this).data("id");
    if (!$(this).hasClass("active")) {
        var m = $(this).parent().find(".J_menuTab.active").data("id");
        spage(m, "out");
        $(".J_mainContent .J_iframe").each(function() {
            if ($(this).data("id") == k) {
                $(this).show().siblings(".J_iframe").hide();
                spage(k, "in");
                return false;
            }
        });
        $(this).addClass("active").siblings(".J_menuTab").removeClass("active");
        g(this);
    }
    selectTab(k);
}
$(".J_menuTabs").on("click", ".J_menuTab", e);

function d() {
    var l = $('.J_iframe[data-id="' + $(this).data("id") + '"]');
    var k = l.attr("src");
}
$(".J_menuTabs").on("dblclick", ".J_menuTab", d);
$(".J_tabLeft").on("click", a);
$(".J_tabRight").on("click", b);
$(".J_tabCloseAll").on("click", function() {
    $(".page-tabs-content").children("[data-id]").not(":first").each(function() {
        var dataId = $(this).data("id");
        var iframe = $('.J_iframe[data-id="' + dataId + '"]');
        iframe.remove();
        $(this).remove();
        spage(dataId, 'destroy');
    });
    $(".page-tabs-content").children("[data-id]:first").each(function() {
        var dataId = $(this).data("id");
        $('.J_iframe[data-id="' + dataId + '"]').show();
        $(this).addClass("active");
        selectTab($(this).data("id"));
        spage(dataId, 'in');
    });
    $(".page-tabs-content").css("margin-left", "0");
});