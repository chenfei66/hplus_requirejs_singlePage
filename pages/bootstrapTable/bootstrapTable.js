define(['bootstrapTable', 'bootstrapTableZh'], function() {

    // var colStyle = function(value, row, index) {
    //     return {
    //         css: {
    //             "text-overflow": "ellipsis",
    //             "white-space": "nowrap",
    //             "overflow": "hidden"
    //         }
    //     };
    // }

    // var FirstFormatter = function(value, row, index) {
    //     return `<span data-toggle="tooltip" data-placement="left" title="${value}">${value}</span>`;
    // }

    var testData = [{
        Tid: "1",
        First: "JavaScript一种直译式脚本语言，是一种动态类型、弱类型、基于原型的语言，内置支持类型。",
        sex: "男",
        Score: "50"
    }, {
        Tid: "2",
        First: "灞波儿奔",
        sex: "男",
        Score: "94"
    }];
    // $("#exampleTableFromData").bootstrapTable({
    //     data: testData
    // });
    // $('span[data-toggle="tooltip"]').tooltip(); // bootstrap table渲染完成以后执行

    $("#exampleTableColumns").bootstrapTable({
        url: "./pages/bootstrapTable/bootstrap_table_test.json",
        height: "400",
        iconSize: "outline",
        showColumns: !0,
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    buildTable($("#exampleTableLargeColumns"), 50, 50);

    $("#exampleTableToolbar").bootstrapTable({
        url: "./pages/bootstrapTable/bootstrap_table_test2.json",
        search: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        toolbar: "#exampleToolbar",
        iconSize: "outline",
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    $("#exampleTableEvents").bootstrapTable({
        url: "./pages/bootstrapTable/bootstrap_table_test.json",
        search: !0,
        pagination: !0,
        showRefresh: !0,
        showToggle: !0,
        showColumns: !0,
        iconSize: "outline",
        toolbar: "#exampleTableEventsToolbar",
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        }
    });
    var e = $("#examplebtTableEventsResult");
    $("#exampleTableEvents").on("all.bs.table", function(e, t, o) {
    }).on("click-row.bs.table", function() {
        e.text("Event: click-row.bs.table");
    }).on("dbl-click-row.bs.table", function() {
        e.text("Event: dbl-click-row.bs.table");
    }).on("sort.bs.table", function() {
        e.text("Event: sort.bs.table");
    }).on("check.bs.table", function() {
        e.text("Event: check.bs.table");
    }).on("uncheck.bs.table", function() {
        e.text("Event: uncheck.bs.table");
    }).on("check-all.bs.table", function() {
        e.text("Event: check-all.bs.table");
    }).on("uncheck-all.bs.table", function() {
        e.text("Event: uncheck-all.bs.table");
    }).on("load-success.bs.table", function() {
        e.text("Event: load-success.bs.table");
    }).on("load-error.bs.table", function() {
        e.text("Event: load-error.bs.table");
    }).on("column-switch.bs.table", function() {
        e.text("Event: column-switch.bs.table");
    }).on("page-change.bs.table", function() {
        e.text("Event: page-change.bs.table");
    }).on("search.bs.table", function() {
        e.text("Event: search.bs.table");
    });

    function buildTable(e, t, o) {
        var n, l, s, a = [],
            c = [];
        for (n = 0; t > n; n++) {
            a.push({
                field: "字段" + n,
                title: "单元" + n
            });
        }
        for (n = 0; o > n; n++) {
            for (s = {},
                l = 0; t > l; l++)
                s["字段" + l] = "Row-" + n + "-" + l;
            c.push(s);
        }
        e.bootstrapTable("destroy").bootstrapTable({
            columns: a,
            data: c,
            iconSize: "outline",
            icons: {
                columns: "glyphicon-list"
            }
        });
    }

    return {
        init: function() {
            // 这个方法里面才是页面最好准备的
            $("#exampleTableFromData").bootstrapTable({
                data: testData
            });
            $('span[data-toggle="tooltip"]').tooltip(); // bootstrap table渲染完成以后执行
        },
        in: function() {},
        out: function() {},
        destroy: function() {},
        colStyle: function(value, row, index) {
            return {
                css: {
                    "text-overflow": "ellipsis",
                    "white-space": "nowrap",
                    "overflow": "hidden"
                }
            };
        },
        FirstFormatter: function(value, row, index) {
            return `<span data-toggle="tooltip" data-placement="left" title="${value}">${value}</span>`;
        }
    }
});